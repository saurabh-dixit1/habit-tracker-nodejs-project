
const form = document.getElementById("signup-form");
    const email = document.getElementById("email");
    const password = document.getElementById("password");
    const emailError = document.getElementById("email-error");
    const passwordError = document.getElementById("password-error");
    const confirm_passwordError = document.getElementById("confirm_password-error");
    const confirm_password = document.getElementById("confirm_password");

    email.addEventListener("input", function(event) {
      if (validateEmail(email.value)) {
        emailError.textContent = "";
        email.classList.remove("is-danger");
      } else {
        emailError.innerHTML = "Please enter a valid email address.";
        email.classList.add("is-danger");
      }
    });

    password.addEventListener("input", function(event) {
      if (validatePassword(password.value)) {
        passwordError.textContent = "";
        password.classList.remove("is-danger");
      } else {
        passwordError.innerHTML = 'Password must be at least 8 characters long.';
        password.classList.add("is-danger");
      }
    });
    confirm_password.addEventListener("input", function(event) {
      if (validateConfirm_password(confirm_password.value)) {
        confirm_passwordError.textContent = "";
        confirm_password.classList.remove("is-danger");
      } else {
        confirm_passwordError.innerHTML = "Password must be at least 8 characters long.";
        confirm_password.classList.add("is-danger");
      }
    });
    

function validateEmail(email) {
const emailRegex = /\S+@\S+.\S+/;
return emailRegex.test(email);
}
function validatePassword(password) {
  return password.length >= 8;
}
function validateConfirm_password(confirm_password) {
  return confirm_password.length >= 8;
}
// password

const showPasswordIcon = document.querySelector('.show-password');
const confirm_passwordField = document.querySelector('#confirm_password');
const passwordField = document.querySelector('input[type="password"]');

showPasswordIcon.addEventListener('click', () => {
  if (passwordField.type === 'password') {
    passwordField.type = 'text';
    showPasswordIcon.innerHTML = '<i class="fas fa-eye"></i>';
  } else {
    passwordField.type = 'password';
    showPasswordIcon.innerHTML = '<i class="fas fa-eye-slash"></i>';
  }
});
showPasswordIcon.addEventListener('click', () => {
  if (confirm_passwordField.type === 'password') {
    confirm_passwordField.type = 'text';
    showPasswordIcon.innerHTML = '<i class="fas fa-eye"></i>';
  } else {
    confirm_passwordField.type = 'password';
    showPasswordIcon.innerHTML = '<i class="fas fa-eye-slash"></i>';
  }
});
