const express = require('express');
const router = express.Router();

// import passport
const passport = require('passport');
// importing user controller with forgot password controller
const userController = require('../controllers/user-and-auth-controller');



router.get('/sign-up' , userController.signup)
router.get('/log-in' , userController.login)

// sign up crete route
router.post('/create-user', userController.create);
// sigin session using middleware passport
router.post('/login-user-session',passport.authenticate('local',{
    failureRedirect:'/user/log-in',
    failureFlash: 'Invalid email or password. Please try again.',
    successFlash: true
},
),userController.create_session);
// signOut request
router.get('/sign-out',  userController.destroySession);

// password reset routers
router.get('/auth/forgot-password',userController.showForgotPassword )

// Route to send a password reset email
router.post('/auth/forgot-password/', userController.forgotPassword);
// Route to handle a password reset link
router.get('/auth/reset-password/:token', userController.handleResetPasswordLink);

// Route to handle a password reset form submission
router.post('/auth/reset-password/', userController.resetPassword);

// for google stretgy
router.get('/auth/google-login' ,passport.authenticate('google' ,{scope: ['profile', 'email ']}));
router.get('/auth/google/callback' ,passport.authenticate( 'google',{failureRedirect: '/user/log-in'}), userController.create_session);


module.exports = router;
